var gulp = require('gulp');
var ts = require('gulp-typescript');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var config = {
  ts: {
    src: './src/*.ts',
    dest: './dist',
    options: {
      lib: ['dom', 'es2015'],
      removeComments: true,
      noImplicitAny: true,
      noLib: false,
      noEmitOnError: false,
      target: 'ES5'
    }
  },
  uglify: {
    dest: './dist'
  }
};

gulp.task('default', function() {
  gulp
    .src(config.ts.src)
    .pipe(ts(config.ts.options))
    .pipe(gulp.dest(config.ts.dest))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest(config.uglify.dest));
});

gulp.task('watch', function() {
  var targets = ['./**/*.ts'];
  gulp.watch(targets, ['default']);
});
