interface Window {
  [key: string]: any;
}

interface Branch {
  code: string;
  name: string;
}

interface Bank {
  code: string;
  name: string;
  branch: Branch[];
}

interface Fields {
  [propName: string]: Element | null;
}

class BankJP {
  url_base: string = 'https://bank-jp.gitlab.io/data';

  cache = {};

  banks: Bank[] = [];

  branches: Branch[] = [];

  fields: Fields = {
    bankName: document.querySelector('input[data-bankjp="bankname"]'),
    bankCode: document.querySelector('input[data-bankjp="bankcode"]'),
    branchName: document.querySelector('input[data-bankjp="branchname"]'),
    branchCode: document.querySelector('input[data-bankjp="branchcode"]')
  };

  lists: Fields = {
    bankList: null,
    branchList: null
  };

  constructor() {
    window['$bankjp_bank'] = (data: any) => {
      this.banks = data;
    };

    window['$bankjp_branch'] = (data: any) => {
      this.branches = data;
    };

    this.lists.bankList = document.createElement('datalist');
    this.lists.bankList.id = 'bankjp-bank-list';
    document.body.appendChild(this.lists.bankList);

    this.lists.branchList = document.createElement('datalist');
    this.lists.branchList.id = 'bankjp-branch-list';
    document.body.appendChild(this.lists.branchList);

    this._loadData('/bank.js');
    this.initListeners();
  }

  initFields() {
    this.fields = {
      bankName: document.querySelector('input[data-bankjp="bankname"]'),
      bankCode: document.querySelector('input[data-bankjp="bankcode"]'),
      branchName: document.querySelector('input[data-bankjp="branchname"]'),
      branchCode: document.querySelector('input[data-bankjp="branchcode"]')
    };
    this.initListeners();
  }

  initListeners() {
    let field;

    if ((field = this.fields.bankName)) {
      field.setAttribute('list', 'bankjp-bank-list');
      field.setAttribute('autocomplete', 'autocomplete');
      for (let ev of ['keyup', 'change']) {
        field.addEventListener(ev, this.updateBankName(), false);
      }
    }

    if ((field = this.fields.branchName)) {
      field.setAttribute('list', 'bankjp-branch-list');
      field.setAttribute('autocomplete', 'autocomplete');
      for (let ev of ['keyup', 'change']) {
        field.addEventListener(ev, this.updateBranchName(), false);
      }
    }

    if ((field = this.fields.bankCode)) {
      field.addEventListener('keyup', this.updateBankCode(), false);
    }

    if ((field = this.fields.branchCode)) {
      field.addEventListener('keyup', this.updateBranchCode(), false);
    }
  }

  updateBankName() {
    const self = this;
    return (e: Event) => {
      self._removeOptions(<Element>self.lists.bankList);

      const val = (<HTMLInputElement>e.target).value;
      if (val.length < 1) {
        return;
      }

      let banks = self.banks.filter((bank: Bank) => {
        return bank.name.indexOf(val) != -1;
      });

      if (!banks.length) {
        return;
      }

      const exactMatch =
        banks.length == 1 && banks[0].name == val ? true : false;

      if (exactMatch !== true) {
        banks = banks.slice(0, 10);
        self._addOptions(<Element>self.lists.bankList, banks);
      }

      if (exactMatch === true) {
        if (self.fields.bankCode) {
          (<HTMLInputElement>self.fields.bankCode).value = banks[0].code;
        }

        if (self.fields.branchName) {
          this._loadData(`/${banks[0].code}.js`);
        }
      }
    };
  }

  updateBranchName() {
    const self = this;
    return (e: Event) => {
      self._removeOptions(<Element>self.lists.branchList);

      const val = (<HTMLInputElement>e.target).value;
      if (val.length < 1) {
        return;
      }

      let branches = self.branches.filter((branch: Branch) => {
        return branch.name.indexOf(val) !== -1;
      });

      if (!branches.length) {
        return;
      }

      const exactMatch =
        branches.length == 1 && branches[0].name == val ? true : false;

      if (exactMatch !== true) {
        branches = branches.slice(0, 10);
        self._addOptions(<Element>self.lists.branchList, branches);
      }

      if (exactMatch === true) {
        if (self.fields.branchCode) {
          (<HTMLInputElement>self.fields.branchCode).value = branches[0].code;
        }
      }
    };
  }

  updateBankCode() {
    const self = this;
    return (e: Event) => {
      const val = (<HTMLInputElement>e.target).value;
      if (val.length < 1) {
        return;
      }

      let bank = self.banks.find((bank: Bank) => {
        return bank.code == val;
      });

      if (bank && self.fields.branchName) {
        (<HTMLInputElement>self.fields.bankName).value = bank.name;
        this._loadData(`/${bank.code}.js`);
      }
    };
  }

  updateBranchCode() {
    const self = this;
    return (e: Event) => {
      const val = (<HTMLInputElement>e.target).value;
      if (val.length < 1) {
        return;
      }

      let branch = self.branches.find((branch: Branch) => {
        return branch.code == val;
      });

      if (branch) {
        (<HTMLInputElement>self.fields.branchName).value = branch.name;
      }
    };
  }

  _loadData(path: string) {
    const scriptTag = document.createElement('script');
    scriptTag.setAttribute('type', 'text/javascript');
    scriptTag.setAttribute('charset', 'UTF-8');
    scriptTag.setAttribute('src', `${this.url_base}${path}`);
    document.head.appendChild(scriptTag);
  }

  _addOptions(target: Element, options: any) {
    for (let option of options) {
      let el = document.createElement('option');
      el.value = option.name;
      target.appendChild(el);
    }
  }

  _removeOptions(target: Element) {
    for (var i = target.childNodes.length - 1; i >= 0; i--) {
      target.removeChild(target.childNodes[i]);
    }
  }
}

new BankJP();
