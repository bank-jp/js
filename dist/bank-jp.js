var BankJP = (function () {
    function BankJP() {
        var _this = this;
        this.url_base = 'https://bank-jp.gitlab.io/data';
        this.cache = {};
        this.banks = [];
        this.branches = [];
        this.fields = {
            bankName: document.querySelector('input[data-bankjp="bankname"]'),
            bankCode: document.querySelector('input[data-bankjp="bankcode"]'),
            branchName: document.querySelector('input[data-bankjp="branchname"]'),
            branchCode: document.querySelector('input[data-bankjp="branchcode"]')
        };
        this.lists = {
            bankList: null,
            branchList: null
        };
        window['$bankjp_bank'] = function (data) {
            _this.banks = data;
        };
        window['$bankjp_branch'] = function (data) {
            _this.branches = data;
        };
        this.lists.bankList = document.createElement('datalist');
        this.lists.bankList.id = 'bankjp-bank-list';
        document.body.appendChild(this.lists.bankList);
        this.lists.branchList = document.createElement('datalist');
        this.lists.branchList.id = 'bankjp-branch-list';
        document.body.appendChild(this.lists.branchList);
        this._loadData('/bank.js');
        this.initListeners();
    }
    BankJP.prototype.initFields = function () {
        this.fields = {
            bankName: document.querySelector('input[data-bankjp="bankname"]'),
            bankCode: document.querySelector('input[data-bankjp="bankcode"]'),
            branchName: document.querySelector('input[data-bankjp="branchname"]'),
            branchCode: document.querySelector('input[data-bankjp="branchcode"]')
        };
        this.initListeners();
    };
    BankJP.prototype.initListeners = function () {
        var field;
        if ((field = this.fields.bankName)) {
            field.setAttribute('list', 'bankjp-bank-list');
            field.setAttribute('autocomplete', 'autocomplete');
            for (var _i = 0, _a = ['keyup', 'change']; _i < _a.length; _i++) {
                var ev = _a[_i];
                field.addEventListener(ev, this.updateBankName(), false);
            }
        }
        if ((field = this.fields.branchName)) {
            field.setAttribute('list', 'bankjp-branch-list');
            field.setAttribute('autocomplete', 'autocomplete');
            for (var _b = 0, _c = ['keyup', 'change']; _b < _c.length; _b++) {
                var ev = _c[_b];
                field.addEventListener(ev, this.updateBranchName(), false);
            }
        }
        if ((field = this.fields.bankCode)) {
            field.addEventListener('keyup', this.updateBankCode(), false);
        }
        if ((field = this.fields.branchCode)) {
            field.addEventListener('keyup', this.updateBranchCode(), false);
        }
    };
    BankJP.prototype.updateBankName = function () {
        var _this = this;
        var self = this;
        return function (e) {
            self._removeOptions(self.lists.bankList);
            var val = e.target.value;
            if (val.length < 1) {
                return;
            }
            var banks = self.banks.filter(function (bank) {
                return bank.name.indexOf(val) != -1;
            });
            if (!banks.length) {
                return;
            }
            var exactMatch = banks.length == 1 && banks[0].name == val ? true : false;
            if (exactMatch !== true) {
                banks = banks.slice(0, 10);
                self._addOptions(self.lists.bankList, banks);
            }
            if (exactMatch === true) {
                if (self.fields.bankCode) {
                    self.fields.bankCode.value = banks[0].code;
                }
                if (self.fields.branchName) {
                    _this._loadData("/" + banks[0].code + ".js");
                }
            }
        };
    };
    BankJP.prototype.updateBranchName = function () {
        var self = this;
        return function (e) {
            self._removeOptions(self.lists.branchList);
            var val = e.target.value;
            if (val.length < 1) {
                return;
            }
            var branches = self.branches.filter(function (branch) {
                return branch.name.indexOf(val) !== -1;
            });
            if (!branches.length) {
                return;
            }
            var exactMatch = branches.length == 1 && branches[0].name == val ? true : false;
            if (exactMatch !== true) {
                branches = branches.slice(0, 10);
                self._addOptions(self.lists.branchList, branches);
            }
            if (exactMatch === true) {
                if (self.fields.branchCode) {
                    self.fields.branchCode.value = branches[0].code;
                }
            }
        };
    };
    BankJP.prototype.updateBankCode = function () {
        var _this = this;
        var self = this;
        return function (e) {
            var val = e.target.value;
            if (val.length < 1) {
                return;
            }
            var bank = self.banks.find(function (bank) {
                return bank.code == val;
            });
            if (bank && self.fields.branchName) {
                self.fields.bankName.value = bank.name;
                _this._loadData("/" + bank.code + ".js");
            }
        };
    };
    BankJP.prototype.updateBranchCode = function () {
        var self = this;
        return function (e) {
            var val = e.target.value;
            if (val.length < 1) {
                return;
            }
            var branch = self.branches.find(function (branch) {
                return branch.code == val;
            });
            if (branch) {
                self.fields.branchName.value = branch.name;
            }
        };
    };
    BankJP.prototype._loadData = function (path) {
        var scriptTag = document.createElement('script');
        scriptTag.setAttribute('type', 'text/javascript');
        scriptTag.setAttribute('charset', 'UTF-8');
        scriptTag.setAttribute('src', "" + this.url_base + path);
        document.head.appendChild(scriptTag);
    };
    BankJP.prototype._addOptions = function (target, options) {
        for (var _i = 0, options_1 = options; _i < options_1.length; _i++) {
            var option = options_1[_i];
            var el = document.createElement('option');
            el.value = option.name;
            target.appendChild(el);
        }
    };
    BankJP.prototype._removeOptions = function (target) {
        for (var i = target.childNodes.length - 1; i >= 0; i--) {
            target.removeChild(target.childNodes[i]);
        }
    };
    return BankJP;
}());
new BankJP();
